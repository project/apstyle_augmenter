# AP Stylebook Date Augmenter

## Table of contents

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## Introduction

This module provides a Date Augmenter plugin, that adjusts date output to meet
the Associated Press Stylebook guidelines.


## Requirements

This module requires the [Date Augmenter API](https://www.drupal.org/project/date_augmenter) module, and a compatible formatter, such as the one provided by the [Smart Date](https://www.drupal.org/project/smart_date) module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Once the module is enabled, you will be able to enable the Associated Press
Stylebook augmenter within the configuration of your date field's compatible
formatter. By design the defaults should make the output compliant with the AP
Stylebook by default, but you have the flexibility to turn off specific rules
if necessary. For best results we recommend using the AP Stylebook Smart Date
Format provided by this module, if using Smart Date.


## Maintainers

- Joe Whitsitt - [joewhitsitt](https://www.drupal.org/u/joewhitsitt)
- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
