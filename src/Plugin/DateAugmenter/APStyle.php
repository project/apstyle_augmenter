<?php

namespace Drupal\apstyle_augmenter\Plugin\DateAugmenter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\date_augmenter\DateAugmenter\DateAugmenterPluginBase;
use Drupal\date_augmenter\Plugin\PluginFormTrait;

/**
 * Date Augmenter plugin adjust date formats meet to AP Stylebook guidelines.
 *
 * @DateAugmenter(
 *   id = "apstyle",
 *   label = @Translation("Associated Press Stylebook"),
 *   description = @Translation("Adjust output to meet the Associated Press Stylebook guidelines"),
 *   weight = 0
 * )
 */
class APStyle extends DateAugmenterPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Builds and returns a render array for the task.
   *
   * @param array $output
   *   The existing render array, to be augmented, passed by reference.
   * @param \Drupal\Core\Datetime\DrupalDateTime $start
   *   The object which contains the start time.
   * @param \Drupal\Core\Datetime\DrupalDateTime $end
   *   The optional object which contains the end time.
   * @param array $options
   *   An array of options to further guide output.
   */
  public function augmentOutput(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    $this->applyApStylebook($output, $start, $end, $options);
    if (!empty($output['site_time'])) {
      $this->applyApStylebook($output['site_time'], $start, $end, $options);
    }
  }

  /**
   * Builds and returns a render array for the task.
   *
   * @param array $output
   *   The existing render array, to be augmented, passed by reference.
   * @param Drupal\Core\Datetime\DrupalDateTime $start
   *   The object which contains the start time.
   * @param Drupal\Core\Datetime\DrupalDateTime $end
   *   The optional object which contains the end time.
   * @param array $options
   *   An array of options to further guide output.
   */
  public function applyApStylebook(array &$output, DrupalDateTime $start, DrupalDateTime $end = NULL, array $options = []) {
    $config = $options['settings'] ?? $this->getConfiguration();
    $date_values = ['start' => $start, 'end' => $end];
    // Prefer likely string patterns over regex for performance.
    $word_replacements = [
      'noon' => ['12pm', '12:00pm', '12 pm', '12:00 pm'],
      'midnight' => ['12am', '12:00am', '12 am', '12:00 am'],
    ];
    $time_labels = [
      'noon' => $this->t('noon')->render(),
      'midnight' => $this->t('midnight')->render(),
    ];
    $bare_times = ['12', '12:00'];
    $time_strings = [];
    $date_strings = [];

    foreach ($date_values as $part => $value) {
      $time_strings[$part] = '';
      if (isset($output[$part]['#text']['time']['value']['#markup'])) {
        $time_strings[$part] = &$output[$part]['#text']['time']['value']['#markup'];
      }
      elseif (isset($output[$part]['time']['value']['#markup'])) {
        $time_strings[$part] = &$output[$part]['time']['value']['#markup'];
      }
      if ($time_strings[$part] && $config['word_replace']) {
        // Check for a start string ending with 12, which would mean we have to
        // get the meridien from the end time.
        if ($part == 'start' && (str_ends_with($time_strings[$part], '12') || str_ends_with($time_strings[$part], '12:00'))) {
          $start_meridien = '';
          $end_string = $output['end']['#text']['time']['value']['#markup'] ?? $output['end']['time']['value']['#markup'] ?? '';
          if (strrpos($end_string, 'am') !== FALSE) {
            $start_meridien = 'am';
          }
          if (strrpos($end_string, 'pm') !== FALSE) {
            $start_meridien = 'pm';
          }
          if ($start_meridien) {
            $word = ($start_meridien === 'am') ? 'midnight' : 'noon';
            $time_strings[$part] = str_replace(
              $bare_times,
              $time_labels[$word],
              $time_strings[$part]
            );
          }
        }
        foreach ($word_replacements as $word => $times) {
          $time_strings[$part] = str_ireplace(
            $times,
            $time_labels[$word],
            $time_strings[$part]
          );
        }
      }
      // Convert meridians to be lowercase and include periods.
      if ($time_strings[$part] && $config['meridian']) {
        // Check for php meridian.
        if (preg_match('/[ap]m/i', $time_strings[$part]) === 1) {
          $time_strings[$part] = str_ireplace(
            ['am', 'pm'],
            ['a.m.', 'p.m.'],
            $time_strings[$part]
          );
        }
      }
      if ($config['month_expand']) {
        $date_strings[$part] = '';
        if (isset($output[$part]['#text']['date']['value']['#markup'])) {
          $date_strings[$part] = &$output[$part]['#text']['date']['value']['#markup'];
        }
        elseif (isset($output[$part]['date']['value']['#markup'])) {
          $date_strings[$part] = &$output[$part]['date']['value']['#markup'];
        }
        // @todo Ensure $value is using the correct timezone.
        $month_full = $value?->format('F');
        if (!$date_strings[$part] || !$month_full) {
          // Nothing to do.
          continue;
        }
        $month_abbr = $value?->format('M');
        if (!$month_abbr || strpos($date_strings[$part], $month_abbr) === FALSE) {
          continue;
        }
        if ($month_abbr === 'Sep') {
          // @todo worry about handling other languages?
          $date_strings[$part] = str_replace(
            [$month_full, $month_abbr],
            'Sept.',
            $date_strings[$part]
          );
          // Normalize on an abbreviated month, with one period.
          $date_strings[$part] = str_replace('Sept..', 'Sept.', $date_strings[$part]);
        }
        elseif (strlen($month_full) > 5) {
          $date_strings[$part] = str_replace(
            [$month_full, $month_abbr],
            $month_abbr . '.',
            $date_strings[$part]
          );
          // Normalize on an abbreviated month, with one period.
          $date_strings[$part] = str_replace($month_abbr . '..', $month_abbr . '.', $date_strings[$part]);
        }
        else {
          // Normalize on no period.
          $date_strings[$part] = str_replace($month_abbr . '.', $month_abbr, $date_strings[$part]);
          // Replace abbreviated month with full name.
          $date_strings[$part] = str_replace(
            $month_abbr,
            $month_full,
            $date_strings[$part]
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'meridian' => TRUE,
      'month_expand' => TRUE,
      'word_replace' => TRUE,
    ];
  }

  /**
   * Create configuration fields for the plugin form, or injected directly.
   *
   * @param array $form
   *   The form array.
   * @param array $settings
   *   The setting to use as defaults.
   *
   * @return array
   *   The updated form array.
   */
  public function configurationFields(array $form, ?array $settings) {
    if (empty($settings)) {
      $settings = $this->defaultConfiguration();
    }
    $form['meridian'] = [
      '#title' => $this->t('Convert <em>ante meridiem</em> and <em>post meridiem</em>.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['meridian'],
      '#description' => $this->t('Meridians should use periods and lower case letters. Converts `a` and `A` time formats'),
    ];
    $form['month_expand'] = [
      '#title' => $this->t('Expand months other than Jan., Feb., Aug., Sept., Oct., Nov. and Dec.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['month_expand'],
      '#description' => $this->t('If a different month is being used, replace the abbreviation with the full month name.'),
    ];
    $form['word_replace'] = [
      '#title' => $this->t('Replace times with the words noon and midnight when appropriate.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['month_expand'],
      '#description' => $this->t('Replace times at 12am or 12pm with the appropriate word.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->configurationFields($form, $this->configuration);

    return $form;
  }

}
